import psycopg2
from flask import session, g
import sqlite3

def check_password(username, password):
    g.cursor.execute("SELECT * from WEB_USERS where web_user=? and password=?", (username, password))
    users = g.cursor.fetchall()
    if len(users)==1:
        user = users[0]
        session['id'] = user[0]
        session['login'] = user[1]
        return True
    else:
        return False

def get_data_cats(p):
    g.cursor.execute("SELECT * from CATS")
    # cur.execute("SELECT count(*) from SKILBOX.CATS")
    rows = g.cursor.fetchall()
    return rows[(p-1)*5:(p-1)*5+5]

def clear_session():
    """
    Очищает сессию
    """
    session.pop("id", None)
    session.pop("login", None)
    return True

def connect_db():
    """
    Соединение с БД
    """
    g.db = sqlite3.connect('db_cats.db')
    g.cursor = g.db.cursor()


def disconnect_db():
    """
    Завершение соединения с БД
    """
    try:
        g.cursor.close()
        g.db.close()
    except AttributeError:
        pass