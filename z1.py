"""
Программа размещает крассные и белые машины, так, чтобы рядом с крассной машиной 
было две белых или наоборот рядом с белой машиной было две крассных
"""

x, y = input("Введите через пробел количество красных и белых машин: ").split(" ")
x = int(x)  # Число крассных машин
y = int(y)  # Число белых
minLength = min(x, y)
maxLength = max(x, y)
# Вспомогательные переменные
whole_x = x // 2
remainder_x = x % 2
whole_y = y // 2
remainder_y = y % 2

park = ""
if maxLength / minLength <= 2:
    if x >= y:
        pair_x = [2 for i in range(whole_x)] + [1 for i in range(remainder_x)]  # считаем количество пар сочетаний
        pair_y = [1 for i in range(whole_x + remainder_x)]  # расскидываем белые машинки для крассных
        for i in range(y % (whole_x + remainder_x)):  # расскидываем оставшиеся белые машинки для крассных
            pair_y[i] += 1
        for i in range(whole_x + remainder_x):
            if pair_x[i] + pair_y[i] == 4:
                park += "RWRW"
            elif pair_x[i] + pair_y[i] == 3:
                park += "RWR"
            else:
                park += "RW"
    if y > x:
        pair_y = [2 for i in range(whole_y)] + [1 for i in range(remainder_y)]  # считаем количество пар сочетаний
        pair_x = [1 for i in range(whole_y + remainder_y)]  # расскидываем крассные машинки для белых
        for i in range(x % (whole_y + remainder_y)):  # расскидываем оставшиеся крассные машинки для белых
            pair_x[i] += 1
        for i in range(whole_y + remainder_y):
            if pair_x[i] + pair_y[i] == 4:
                park += "RWRW"
            elif pair_x[i] + pair_y[i] == 3:
                park += "WRW"
            else:
                park += "WR"
else:
    park = "Нет решения"
print(park)
